package ru.helloworld.lab01

import junit.framework.TestCase

class MinMaxTest : TestCase() {

    private val underTest = MinMax()
    
    fun testMin1() {
        assertEquals(3, underTest.min(3, 4))
    }

    fun testMin2() {
        assertEquals(3, underTest.min(4, 3))
    }

    fun testMin3() {
        assertEquals(.3, underTest.min(.3, .4))
    }

    fun testMin4() {
        assertEquals(1, underTest.min(1, 1.05))
    }

    fun testMax1() {
        assertEquals(4, underTest.max(3, 4))
    }

    fun testMax2() {
        assertEquals(4, underTest.max(4, 3))
    }

    fun testMax3() {
        assertEquals(.4, underTest.max(.3, .4))
    }

    fun testMax4() {
        assertEquals(1.05, underTest.max(1, 1.05))
    }
}