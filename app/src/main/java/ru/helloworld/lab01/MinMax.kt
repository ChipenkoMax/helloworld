package ru.helloworld.lab01

class MinMax {

    /**
     * @param a first number
     * @param b second number
     * @return value of whichever of those is less, or their value if equal
     */
    fun min(a: Number, b: Number) = if (a.toDouble() < b.toDouble()) a else b

    /**
     * @param a first number
     * @param b second number
     * @return value of whichever of those is more, or their value if equal
     */
    fun max(a: Number, b: Number) = if (a.toDouble() > b.toDouble()) a else b

}